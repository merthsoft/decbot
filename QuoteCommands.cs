﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace Merthsoft.DecBot3 {
	partial class DecBot {
		private static void DeleteQuote(string messageSender, string channel, string command, IList<string> parameters) {
			if (!HasPrivs(messageSender, channel)) {
				SendMessage(channel, "User not authorized to delete quotes.");
				return;
			}
			int id;
			if (parameters.Count != 1 || !int.TryParse(parameters[0], out id)) {
				SendMessage(channel, "Must supply id of quote to delete.");
				return;
			}

			using (Command = new MySqlCommand(QuoteSqlCommands.DeleteQuote, Connection))
			using (Connection.Open()) {
				Command.Prepare();

				Command.Parameters.AddWithValue("@id", id);
				Command.Parameters.AddWithValue("@name", messageSender);

				Command.ExecuteNonQuery();
				SendMessage(channel, "Deleted quote #{0}.", id);
			}
		}

		private static void AddQuote(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count == 0) {
				SendMessage(channel, "Cannot add empty quote.");
				return;
			}

			string text = string.Join(" ", parameters);
			using (Command = new MySqlCommand() { Connection = Connection })
			using (Connection.Open()) {
				// Get the final name
				messageSender = getLinkedName(messageSender) ?? messageSender;

				Command.ExecuteFullUpdate(QuoteSqlCommands.AddQuote, new { Name = "@name", Value = messageSender }, new { Name = "@quote", Value = text.Trim() });
				SendMessage(channel, "Added quote #{0}.", Command.LastInsertedId);
			}
		}

		private static void GetQuote(string messageSender, string channel, string command, IList<string> parameters) {
			int quoteNumber;
			if (parameters.Count == 0) {
				RandomQuote(messageSender, channel, command, parameters);
				return;
			} else if (parameters.Count > 1 || !int.TryParse(parameters[0], out quoteNumber)) {
				SearchQuote(messageSender, channel, command, parameters);
				return;
			}

			using (Command = new MySqlCommand(QuoteSqlCommands.GetQuoteById, Connection))
			using (Connection.Open()) { 
				Command.Prepare();
				Command.Parameters.AddWithValue("@id", quoteNumber);

				using (var reader = Command.ExecuteReader(CommandBehavior.SingleResult)) {
					if (reader.Read()) {
						Quote q = new Quote(reader);
						SendMessage(channel, q.ToString());
					} else {
						SendMessage(channel, "No quote found with id {0}.", quoteNumber);
					}
				}
			}
		}

		private static void SearchQuote(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count == 0) {
				SendMessage(channel, "Cannot add search for empty string.");
				return;
			}

			string text = string.Format("%{0}%", string.Join(" ", parameters));

			using (Command = new MySqlCommand(QuoteSqlCommands.SearchQuote, Connection))
			using (Connection.Open()) {
				Command.Prepare();
				Command.Parameters.AddWithValue("@search", text);

				using (var reader = Command.ExecuteReader(CommandBehavior.SingleResult)) {
					if (reader.HasRows) {
						reader.Read();
						Quote q = new Quote(reader);
						List<int> ids = new List<int>() {q.Id};
						int numQuotes = 1;
						while (reader.Read()) {
							numQuotes++;
							ids.Add((int)reader["Id"]);
						}
						if (numQuotes == 1) {
							SendMessage(channel, "1 quote found: {0}", q);
						} else {
							SendMessage(channel, "{0} quotes found: {1}. Top quote: {2}", numQuotes, string.Join(", ", ids), q);
						}
					} else {
						SendMessage(channel, "No quote found matching {0}.", text);
					}
				}
			}
		}

		private static void RandomQuote(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count > 0) {
				SendMessage(channel, "Parameters ignored for random quote.");
			}

			using (Command = new MySqlCommand(QuoteSqlCommands.RandomQuote, Connection))
			using (Connection.Open()) {
				Command.Prepare();

				using (var reader = Command.ExecuteReader(CommandBehavior.SingleResult)) {
					if (reader.Read()) {
						Quote q = new Quote(reader);
						SendMessage(channel, q.ToString());
					} else {
						SendMessage(channel, "No quotes found.");
					}
				}
			}
		}
	}
}
