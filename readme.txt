﻿Merthsoft.DecBot3
Merthsoft Creations, 2014
Shaun McFall

A karma-tracking IRC bot.

Requires the following references:
SmartIRC4Net - https://www.meebey.net/projects/smartirc4net/ (Needed for IRC functionality)
StarkSoftProxy - https://github.com/meebey/starksoftproxy (SmartIRC4Net needs this)
MySql Connectors - http://dev.mysql.com/downloads/connector/net/ (Needed for SQL communication)
Merthsoft.TinyUrlApi - https://bitbucket.org/merthsoft/tinyurlapi (Needed for below APIs)
Merthsoft.DynamicConfig - https://bitbucket.org/merthsoft/dynamicconfig (Needed for loading in INI files)
Merthsoft.WolframAlphaApi - https://bitbucket.org/merthsoft/wolframalphaapi (Needed for !calc command--calculator)
Merthsoft.WundergroundApi - https://bitbucket.org/merthsoft/wundergroundapi (Needed for !weather command--current weather conditions)

I'm not going to bother including all the DLLs and such. You'll have to go get them on your own. Sorry!

As for documentation... Code should be self-documenting? Let me know if you have any questions, otherwise good luck!